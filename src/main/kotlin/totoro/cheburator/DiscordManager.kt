package totoro.cheburator

import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class DiscordManager(token: String, private val channel: String): Manager() {
    private val jda = JDABuilder(token)
        .addEventListeners(object: ListenerAdapter() {
            @Override
            override fun onMessageReceived(event: MessageReceivedEvent) {
                if (listener != null) {
                    listener?.invoke(Message(event.author.name, event.message.contentDisplay))
                }
            }
        })
        .setActivity(Activity.watching("IRC"))
        .build()

    override fun send(message: Message) {
        jda.getTextChannelsByName(channel, false).map {
            it.sendMessage("**${message.author}**: ${message.text}").submit()
        }
    }
}
