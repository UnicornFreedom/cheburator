package totoro.cheburator

import net.engio.mbassy.listener.Handler
import org.kitteh.irc.client.library.Client
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent

class IrcManager(host: String, private val channel: String, nickname: String): Manager() {
    private val client: Client = Client.builder()
        .nick(nickname)
        .server()
        .host(host)
        .then().buildAndConnect()

    init {
        client.eventManager.registerEventListener(this)
        client.addChannel(channel)
    }

    override fun send(message: Message) {
        client.sendMessage(channel, "\u000304${message.author}\u000F: ${message.text}")
    }

    @Handler
    fun incoming(event: ChannelMessageEvent) {
        if (listener != null) {
            listener?.invoke(Message(event.actor.nick, event.message))
        }
    }
}
