package totoro.cheburator

import java.io.FileInputStream
import java.util.*

object Config {
    private val prop = Properties()

    private const val NICKNAME_KEY = "nickname"
    private const val IRC_CHANNEL_KEY = "irc-channel"
    private const val IRC_HOST_KEY = "irc-host"
    private const val DISCORD_CHANNEL_KEY = "discord-channel"
    private const val TOKEN_KEY = "token"

    var nickname: String? = null
    var irc_channel: String? = null
    var irc_host: String? = null
    var discord_channel: String? = null
    var token: String? = null

    fun load(filename: String) {
        FileInputStream(filename).use {
            prop.load(it)
            nickname = getString(NICKNAME_KEY, "cheburator")
            irc_channel = getString(IRC_CHANNEL_KEY, "#cc.ru")
            irc_host = getString(IRC_HOST_KEY, "irc.esper.net")
            discord_channel = getString(DISCORD_CHANNEL_KEY, "irc-discord")
            token = getString(TOKEN_KEY, null)
        }
    }

    private fun getString(key: String, default: String?): String? {
        return prop.getProperty(key) ?: default
    }
}
