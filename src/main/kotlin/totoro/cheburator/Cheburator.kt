package totoro.cheburator

class Cheburator {
    init {
        Config.load("cheburator.properties")
        val ircManager = IrcManager(Config.irc_host!!, Config.irc_channel!!, Config.nickname!!)
        val discordManager = DiscordManager(Config.token!!, Config.discord_channel!!)
        ircManager.subscribe(discordManager::send)
        discordManager.subscribe(ircManager::send)
    }
}

fun main() {
    Cheburator()
}
