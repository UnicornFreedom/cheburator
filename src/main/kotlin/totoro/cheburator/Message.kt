package totoro.cheburator

/**
 * Это просто общий внутренний формат сообщений, не привязанный ни к какому конкретному чату.
 */
class Message(val author: String, val text: String)
